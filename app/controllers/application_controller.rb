class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
      render html: "Hola world!"
  end
  def goodbay
      render html: "Goodbay world!"
  end
  
end
